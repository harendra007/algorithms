/**
 * Requires jQuery
 */

function Node(){
	this.value=null;
	this.height=null;
	this.width=null;
	this.left=null;
	this.right=null;
	
}
function PlotBST(){
	var canvasid="mycanvas";
	var root=null;
	var canv_width=1024;
	var canv_height=1024;
	var levelheight=100;
	
	function createCanvas(){
		var width="1024px";
		var height="1024px";
		
		var canvas=$("<canvas></canvas>");
		$(canvas).attr({"id":canvasid,"width":width,"height":height});
		$('body').append(canvas);
		
	}
	
	function drawNode(parentNode,childNode){
		console.log(childNode.value);
		//console.log(childNode.height);
		//console.log(childNode.width);
		var canvas = document.getElementById(canvasid);
		var ctx = canvas.getContext("2d");
		var w = 40;
		var x = childNode.width;
		var y = childNode.height
		ctx.beginPath();
		ctx.fillStyle = "rgb(0,0,0)";
		ctx.arc(x, y, w/2, 0, 2 * Math.PI, false);
		ctx.stroke();
		ctx.fill();

		ctx = canvas.getContext("2d");
		ctx.font = '16pt Calibri';
		ctx.fillStyle = 'white';
		ctx.textAlign = 'center';
		ctx.fillText(childNode.value, x, y+3);
		
		if(parentNode!=null){
			//ctx.beginPath();
			ctx.moveTo(parentNode.width,parentNode.height+w/2);
			ctx.lineTo(childNode.width	,childNode.height-w/2);
			ctx.stroke();
		}
		
	}
	
	function getDifferential(height){
		var level=height/levelheight;
		var initial=1024;
		for(var i=0;i<=level;i++){
			initial=initial/2;
		}
		return initial;
		
	}
	
	function insertNode(parentnode,value){
		if(root==null){
			root=new Node();
			root.value=value;
			root.height=levelheight;
			root.width=canv_width/2;
			drawNode(null,root);
			return;
		}
		if(value<parentnode.value){
			if(parentnode.left!=null){
				insertNode(parentnode.left,value);
			}
			else{
				var currentnode=new Node();
				currentnode.value=value;
				currentnode.height=parentnode.height+levelheight;
				currentnode.width=parentnode.width-getDifferential(parentnode.height);
				parentnode.left=currentnode;
				drawNode(parentnode,currentnode);
				return
			}

		}
		else if(value>parentnode.value){
			if(parentnode.right!=null){
				insertNode(parentnode.right,value);
			}
			else{
				var currentnode=new Node();
				currentnode.value=value;
				currentnode.height=parentnode.height+levelheight;
				currentnode.width=parentnode.width+getDifferential(parentnode.height)
				parentnode.right=currentnode;
				drawNode(parentnode,currentnode);
				return
			}
			
		}
	
	}
	
	this.initialize=function(){
		//create canvas element
		createCanvas();
		var values=[20,3,5,7,89,23,56,78,93,4,78,90,26,75]
		for(var i=0;i<values.length;i++){
			insertNode(root,values[i]);
		}
		
		
	}
}